<?php 
namespace BDAA;

include_once('Base.php');
include_once('District.php');

use BDAA\Base as Base;
use BDAA\District as District;
/**
 * This class represents a Division - the first child on the whole administrative system
 * 
 * 
 * @package Bangladesh_Administrative_Areas
 * @author Md Abdullah Al Maruf <maruf.sylhet@gmail.com>
 * @version 1.0
 
 * @see Abstract.php
 * @see District.php
 */

class Division extends Base {
    
    protected $iso_3166_2;
    protected $name_bn;
    protected $name;
    protected $area;
    protected $population_density;
    protected $sex_ratio;
    protected $population_1991;
    protected $population_2001;
    protected $population_2011;
    protected $districts;

    /**
    * The setup process of the object
    *
    * @see      Base.php
    * @param    array  $options     The object properties 
    */   
    public function __construct($options = null) {
        parent::__construct($options);
    }

    
    /**
    * Get the array of District objects for this Division
    *
    * @return   array   The array of \BDAA\District objects
    */
    public function getDistricts() {
        if (is_array($this->districts) && !(end($this->districts)) instanceof District) {
        
            foreach($this->districts as $disrictName => $districtDetails) {
                $districtDetails['name'] = $disrictName;
                $this->districts[ $disrictName ] = new District($districtDetails);                
                $this->districts[ $disrictName ]->getThanas();
            }
        }
        
        return $this->districts;
    }
    
    
    /**
    * Get the ISO 3166_2 code for this Division
    *
    * @return   string 
    */
    public function getIso31662() {
        return $this->iso_3166_2;
    }
    
    /**
    * Alias for getIso31662()
    *
    * @return   string 
    */
    public function getId() {
        return $this->getIso31662();
    }    
    
    /**
    * Get the Division name in English
    *
    * @return   string 
    */
    public function getName() {
        return $this->name;
    }
        
    /**
    * Get the Division name in Bangla
    *
    * @return   string 
    */
    public function getNameBn() {
        return $this->name_bn;
    }
    
    /**
    * Get the total area of the Division in squire kilometre
    *
    * @return   string 
    */
    public function getArea() {
        return $this->area;
    }
    
    /**
    * Get the Division population density per squire kilometre
    *
    * @return   string
    */
    public function getPopulationDensity() {
        return $this->population_density;
    }
    
    /**
    * Get the percentage of male/female 
    *
    * @return   string 
    */
    public function getSexRatio() {
        return $this->sex_ratio;
    }
    
    /**
    * Get the population in 1991
    *
    * @return   string 
    */
    public function getPopulation1991() {
        return $this->population_1991;
    }
    
    /**
    * Get the population in 2001
    *
    * @return   string 
    */
    public function getPopulation2001() {
        return $this->population_2001;
    }
        
    /**
    * Get the population in 2011
    *
    * @return   string 
    */
    public function getPopulation2011() {
        return $this->population_2011;
    }    
}
